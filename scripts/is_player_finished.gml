//is_player_finished()

var obj_gamekeeper.percentage = 0;

with(obj_player)
{
    if(position_meeting(target_x,target_y,obj_exit_tile))
    {
        obj_gamekeeper.percentage += mind_percentage;
    }
}

if(obj_gamekeeper.percentage<obj_gamekeeper.required_percentage)
{
    return false;
}
else
{
    return true;
}
