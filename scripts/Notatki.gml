
//I can see clearly now - The game has high contrast visuals, with a contrast ratio of at least 4.5 : 1 (contrast ratio checker)
//Stranger Things - Your game's ambience is inspired by an 80s pop song
//Beatbox - All sounds for your game must be created using your voice or body
//A Bold Choice - Compose the music in a style that is completely inappropriate for your game
//I’ll be there in a minute - Your game can be played through in 30 seconds or less.
//Created by Warren Robinett  - Your game contains some kind of hidden secret or Easter egg.
//Hidden Depths - The protagonist isn’t who you think they are at the beginning.
//Final Countdown - Create AN ADDITIONAL game built and finished in the last hour of GGJ
//Inception - Create a totally different game that can be played inside your game submission. Must also relate to the theme.
